document.addEventListener('DOMContentLoaded', function(){
    canvas = document.createElement('canvas')
    let ctx = canvas.getContext('2d')
    document.body.appendChild(canvas)

    canvas.height = 400
    canvas.width = 400
    canvas.setAttribute('style', 'border: 1px solid red')

    redBox = {x: 0, y: 0, width: 25, height: 25, color: 'red'}
    blueBox = {x: 0, y: 0, width: 25, height: 25, color: 'blue'}

    box = redBox

    startX = 5
    startY = 5
    padding = 5

    for (var i = 0; i < 5; i++) {
        x = startX  + (box.width + padding) * i
        y = startY

        width = box.width
        height = box.height
        color = box.color
        drawBox(ctx, x, y, width, height, color)
    }

    box = blueBox

    startX = 5
    startY = 5
    padding = 5

    for (var i = 0; i < 5; i++) {
        x = startX
        y = startY + (box.width + padding) * i
        width = box.width
        height = box.height
        color = box.color
        drawBox(ctx, x, y, width, height, color)
    }
})

function drawBox(context, x, y, width, height, color) {
    context.fillStyle = color
    context.fillRect(x, y, width, height)
}

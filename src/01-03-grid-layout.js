document.addEventListener('DOMContentLoaded', function() {
    canvas = document.createElement('canvas')
    ctx = canvas.getContext('2d')

    document.body.appendChild(canvas)

    canvas.height = 400
    canvas.width = 400
    canvas.setAttribute('style', 'border: 1px solid red')

    // redBox = {x: 0, y: 0, width: 25, height: 25, color: 'red'}
    // blueBox = {x: 0, y: 0, width: 25, height: 25, color: 'blue'}
    //
    // box = redBox

    startX = 0
    startY = 0
    padding = 2

    cols = 10
    rows = 10
    total  = cols * rows

    width = canvas.width / cols
    height = canvas.height / rows

    for (var i = 0; i < rows; i++) {
        for (var j = 0; j < cols; j++) {
            x = startX + (width * j) + padding
            y = startY + (height * i) + padding
            b_width = width - 2 * padding
            b_height = height - 2 * padding

            drawBox(ctx, x, y, b_width, b_height, 'red')
        }
    }
})

function drawBox(context, x, y, width, height, color) {
    context.fillStyle = color
    context.fillRect(x, y, width, height)
}
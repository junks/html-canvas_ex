document.addEventListener('DOMContentLoaded', function() {
    const canvas = document.createElement('canvas')

    document.body.appendChild(canvas)

    canvas.height = 400
    canvas.width = 400

    //constants:
    const redBox = {x: 0, y: 0, width: 150, height: 150, color: 'red'}
    const blueBox = {x: 0, y: 0, width: 25, height: 25, color: 'blue'}

    canvas.setAttribute('style', 'border: 1px solid red;')

    const context = canvas.getContext('2d')

    redBox.x = 200 - (.5 * redBox.width)
    redBox.y = 200 - (.5 * redBox.height)

    fillRect(context, redBox.x, redBox.y, redBox.width, redBox.height, redBox.color)

    canvas.onmousemove = function (e) {
        canvas.width = canvas.width

        blueBox.x = e.offsetX - (.5 * blueBox.width)
        blueBox.y = e.offsetY - (.5 * blueBox.height)

        fillRect(context, redBox.x, redBox.y, redBox.width, redBox.height, redBox.color)
        fillRect(context, blueBox.x, blueBox.y, blueBox.width, blueBox.height, blueBox.color)

        const collision = contains(redBox, blueBox)

        blueBox.color = collision ? 'white' : 'blue'

        fillText(context, 'Collision: ' + collision, 10, 10, null, 'red')

    }
})

function fillRect(context, x, y , width, height, color='white') {
    context.fillStyle = color
    context.fillRect(x, y, width, height)
}

function fillText(context, text, x, y , font='38px sans-serif', color = 'white') {
    context.fillStyle = color
    context.font= font
    context.fillText(text, x, y)
}

function contains(boundary, target) {
    return (
        (target.x + target.width) > boundary.x
        && (target.y + target.height) > boundary.y
        && target.x < (boundary.x + boundary.width)
        && target.y < (boundary.y + boundary.height)
    )
}

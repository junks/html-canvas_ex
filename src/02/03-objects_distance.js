document.addEventListener('DOMContentLoaded', function() {
    const canvas = document.createElement('canvas')

    canvas.height = 600
    canvas.width = 600
    canvas.setAttribute('style', 'border: 1px solid blue')

    document.body.appendChild(canvas)

    const context = canvas.getContext('2d')

    canvas.onmousemove = (e=> {
       canvas.height=600
        drawBox(context, 250, 250, 100, 100, 'blue')
        drawBox(context, e.offsetX - 50, e.offsetY - 50, 100, 100)

        drawDashLine(context, e.offsetX, e.offsetY, 300, 300)
        drawText(context, .5 * Math.abs(e.offsetX + 300), .5 * Math.abs(e.offsetY + 300), 'text')
    })
})

function drawBox(context, x, y, w, h, color = 'red') {
    context.fillStyle = color
    context.fillRect(x, y, w,h)
}

function drawDashLine(context, x1, y1, x2, y2) {
    context.beginPath()
    context.moveTo(x1, y1)
    context.lineTo(x2, y1)
    context.lineTo(x2, y2)
    context.setLineDash([5,10])
    context.stroke()
    context.beginPath()
    context.moveTo(x2, y2)
    context.lineTo(x1, y1)
    context.setLineDash([])
    context.stroke()
}

function drawText(context, x, y, text, color = 'black') {
    context.fillStyle = color
    context.fillText(text, x, y)
}

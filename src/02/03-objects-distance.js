document.addEventListener('DOMContentLoaded', function () {
    const canvas_w = 600, canvas_h = 600
    const canvas = document.createElement('canvas')
    document.body.appendChild(canvas)
    const context = canvas.getContext('2d')
    const redBox = {x: canvas_w * .5 - .5 * 150, y: canvas_h * .5 - .5 * 150, height: 150, width: 150, color: 'red'}
    const blueBox = {x: 0, y: 0, height: 25, width: 25, color: 'blue'}

    canvas.height = canvas_h
    canvas.width = canvas_w

    canvas.setAttribute('style', 'border: 1px solid red;')

    fillRect(context, redBox.x, redBox.y, redBox.width, redBox.height, redBox.color)

    canvas.onmousemove = function (e) {
        canvas.width = canvas.width
        fillRect(context, redBox.x, redBox.y, redBox.width, redBox.height, redBox.color)

        blueBox.x = e.offsetX - blueBox.width * .5
        blueBox.y = e.offsetY - blueBox.height * .5

        fillRect(context, blueBox.x, blueBox.y, blueBox.width, blueBox.height, blueBox.color)

        const xd = e.offsetX - (redBox.x + redBox.width * .5)
        const yd = e.offsetY - (redBox.y + redBox.height * .5)
        const distance = Math.sqrt((xd * xd ) + (yd * yd)).toFixed(2)

        fillText(context, 'Distance: ' + distance , 10, 20)

        drawLine(context, redBox.x + redBox.width * .5, redBox.y + redBox.height * .5, e.offsetX, e.offsetY)
        drawDashLine(context, canvas.width * .5, canvas.height * .5, canvas.width * .5, e.offsetY)
        drawDashLine(context, canvas.width * .5, e.offsetY, e.offsetX, e.offsetY)
    }

})

function fillText(context, text, x, y, font = "14px sans-serif", color = 'red') {
    context.fillStyle = color
    context.font = font
    context.fillText(text, x, y)
}

function fillRect(context, x, y, width, height, color = 'white') {
    context.fillStyle = color
    context.fillRect(x, y, width, height)
}

function drawDashLine(context, ori_x = 0, ori_y = 0, end_x = 0, end_y = 0) {
    context.setLineDash([5, 10])
    context.beginPath()
    context.moveTo(ori_x, ori_y)
    context.lineTo(end_x, end_y)
    context.stroke()
}

function drawLine(context, ori_x = 0, ori_y = 0, end_x = 0, end_y = 0) {
    context.beginPath()
    context.moveTo(ori_x, ori_y)
    context.lineTo(end_x, end_y)
    context.stroke()
}

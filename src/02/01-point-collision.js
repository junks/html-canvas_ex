document.addEventListener('DOMContentLoaded', function () {
    canvas = document.createElement('canvas')
    document.body.appendChild(canvas)

    ctx = canvas.getContext('2d')

    canvas.height = 400
    canvas.width = 400

    canvas.setAttribute('style', 'border: 1px solid red')

    redBox = {x: 0, y: 0, width: 150, height: 150, color: 'red'}
    blueBox = {x: 0, y: 0, width: 150, height: 150, color: 'blue'}

    box = redBox
    fillRect(ctx, box.x, box.y, box.width, box.height, box.color)

    canvas.onmousemove = (function(e) {
        offsetX = e.offsetX
        offsetY = e.offsetY

        var collision = contains(box, offsetX, offsetY)

        text = 'Collision '+collision+' mouse('+offsetX+','+offsetY+')'

        color = collision ? 'black' : 'silver'
        fillText(ctx, text, 200, 25, null, color)
        fillRect(ctx, box.x, box.y, box.width, box.height, color)
        console.log(text)
    })
})

function fillText(context, text, x, y, font='38px sans-serif', color='white'){
    context.fillStyle = color
    context.font = font
    context.fillText(text, x, y)
}

function fillRect(context, x, y, width, height, color='white') {
    context.fillStyle = color
    context.fillRect(x, y, width, height)
}

function contains(target, x, y) {
   return (x > target.x && y > target.y && x < (target.x + target.width) && y < (target.y + target.height))
}

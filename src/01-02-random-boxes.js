document.addEventListener('DOMContentLoaded', function() {
    canvas = document.createElement('canvas')

    document.body.append(canvas)

    canvas.height = 400
    canvas.width = 400

    redBox = {x: 0, y: 0, width: 25, height: 25, color: 'red'}
    blueBox = {x: 0, y: 0, width: 25, height: 25, color: 'blue'}

    canvas.setAttribute('style', 'border: 1px solid red')

    ctx = canvas.getContext('2d')


    box = redBox

    for (var i = 0; i < 15; i++) {
        drawBox(ctx, randomInRange(canvas.width), randomInRange(canvas.height), box.width, box.height, box.color)
    }

    box = blueBox

    for (var i = 0; i < 15; i++) {
        drawBox(ctx, randomInRange(canvas.width), randomInRange(canvas.height), box.width, box.height, box.color)
    }

})

function drawBox(context, x, y, width, height, color) {
    context.fillStyle = color
    context.fillRect(x, y, width, height)
}

function randomInRange(n) {
    return Math.floor(Math.random() * n)
}

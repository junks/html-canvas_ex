document.addEventListener('DOMContentLoaded', function() {
   const canvas = document.createElement('canvas')
    canvas.width = 600
    canvas.height = 600

    canvas.setAttribute('style', 'border: 1px solid black')

    document.body.appendChild(canvas)

    const context = canvas.getContext('2d')

    strokeRect(context, health_bar_dimensions.x, health_bar_dimensions.y, health_bar_dimensions.width, health_bar_dimensions.height)

    fillRect(context, health_bar_dimensions.x + 1, health_bar_dimensions.y + 1, health_bar_dimensions.width - 2, health_bar_dimensions.height - 2)

    const form = document.getElementById('health_form')

    form.onsubmit = e => {
       canvas.height = 600
       e.preventDefault()
        strokeRect(context, health_bar_dimensions.x, health_bar_dimensions.y, health_bar_dimensions.width, health_bar_dimensions.height)

        const health = document.getElementById('health_input').value

        const ratio = health_bar_dimensions.width * .01

        console.log('health: ', isNaN(health))

        if (!isNaN(health)){
            if (parseInt(health) * ratio < health_bar_dimensions.width) {
                fillRect(context, health_bar_dimensions.x + 1, health_bar_dimensions.y + 1, parseInt(health) * ratio, health_bar_dimensions.height)
            } else {
                fillRect(context, health_bar_dimensions.x + 1, health_bar_dimensions.y + 1, health_bar_dimensions.width - 2, health_bar_dimensions.height - 2)
            }
        }
   }
})


const health_bar_dimensions = {
    x: 30,
    y: 10,
    width: 200,
    height: 25
}

function strokeRect(context, x, y, w, h, color='black') {
    context.fillStyle = color
    context.strokeRect(x, y, w, h)
}

function fillRect(context, x, y, w, h, color='red') {
    context.fillStyle = color
    context.fillRect(x, y, w, h)
}

document.addEventListener('DOMContentLoaded', function() {
    canvas = document.createElement('canvas')

    if (!document.body) {
        alert('DOM content is missing')
        return
    }
    document.body.append(canvas)

    canvas.height = 400
    canvas.width = 400
    canvas.setAttribute('style', 'border: 1px solid red')

    ctx = canvas.getContext('2d')

    map = {x : 0, y: 0, width: 200, height: 200, color: 'maroon'}
    player = {x: 0, y: 0, width: 25, height: 25, color: 'silver'}

    centerObject(player, map)

    fillRect(ctx, map.x, map.y, map.width, map.height, map.color)
    fillRect(ctx, player.x, player.y, player.width, player.height, player.color)
})

function fillRect(context, x, y, width, height, color='white') {
    context.fillStyle = color
    context.fillRect(x, y, width, height)
}

function centerObject(obj1, obj2) {
    obj1.x = obj2.x + (obj2.width - obj1.width) * .5
    obj1.y = obj2.y + (obj2.height - obj1.height) * .5
}
